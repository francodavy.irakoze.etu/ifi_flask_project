from datetime import datetime
from flask import Flask, render_template, url_for, flash, redirect
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user
from forms import RegistrationForm, LoginForm, AddPostForm

app = Flask(__name__)

app.config['SECRET_KEY'] = 'skdfjmdknmjhdfmgf5f4654g5g4654'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///site.db'

db = SQLAlchemy(app)
login_manager = LoginManager(app)

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    content = db.Column(db.Text, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

    def __repr__(self):
        return "Post(%s)" % self.title

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), nullable=False, unique=True)
    email = db.Column(db.String(120), nullable=False, default=datetime.utcnow, unique=True)
    password = db.Column(db.String(60), nullable=False)
    posts = db.relationship('Post', backref='author', lazy=True)

    def __repr__(self):
        return "User(%s)" % self.username


@app.route("/")
@app.route("/home")
def home():
    return render_template("home.html", posts=Post.query.all(), title="Home Page")


@app.route("/post", methods=['GET', 'POST'])
@login_required
def add_post():
    form = AddPostForm()
    if form.validate_on_submit():
        db.session.add(Post(title=form.title.data, content=form.content.data, author=current_user))
        db.session.commit()
        flash('Your post has been created !')
        return redirect(url_for('home'))
    return render_template("post.html", title="New article", form=form)


@app.route("/about")
def about():
    return render_template("about.html", title="About")


@app.route("/login", methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and user.password == form.password.data:
            login_user(user, remember=form.remember.data)
            flash("You are now logged as  %s !" % user.username)
            return redirect(url_for('home'))
        else:
            flash('Login unsucessful. Please check email and password')
    return render_template("login.html", title='Login', form=form)


@app.route("/register", methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, password=form.password.data, email=form.email.data)
        db.session.add(user)
        db.session.commit()
        flash("Account created  for %s !" % form.username.data)
        return redirect(url_for('home'))
    return render_template("register.html", title='Register', form=form)


@app.route("/logout")
@login_required
def logout():
    logout_user()
    flash('Your have been logged out !')
    return redirect('home')


if __name__ == "__main__":
    db.create_all()
    app.run(debug=True)
